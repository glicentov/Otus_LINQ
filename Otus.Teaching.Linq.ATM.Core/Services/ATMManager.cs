﻿using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        private int _accountId = default; // Id счёта пользователя
        private List<Account> _userAccounts = new List<Account>(); // Счета пользователя 

        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }
        
        //TODO: Добавить методы получения данных для банкомата
        public void GetUsersByAccountAndPass(string login, string password)
        {
            var filteredUser = this.Users.Where(user => user.Login == login && user.Password == password).First();
            System.Console.WriteLine($"User Name: {filteredUser.FirstName} User AccountID: {filteredUser.Id}");
            _accountId = filteredUser.Id;// Запоминаем id счёта из аккаунта пользователя
        }
        public void GetAccountsByUser()
        {
            List<Account> userAccounts = this.Accounts.Where(account => account.UserId == this._accountId).ToList();
            foreach (var userAccount in userAccounts)
            {
                System.Console.WriteLine($"AccountID: {userAccount.Id} OpeningDate: {userAccount.OpeningDate} CashAll: {userAccount.CashAll}");
            }
            _userAccounts = userAccounts; // Сохраняем счета в приватную переменную чтобы не считывать их в других методах ещё раз
        }
        public void GetUserAccountsWithHistory()
        {
            var history = Accounts.Where(acc => acc.UserId == _accountId).
                          Join(History,
                               acc => acc.Id,
                               hist => hist.AccountId,
                               (acc, hist) => new { Account = acc, History = hist });
            System.Console.WriteLine($"AccountID: {history.First().Account.Id}  see below the last operations\n"); // выводим номер счёта
            foreach (var curHistRec in history)
            {
                System.Console.WriteLine($"Start===>\nOperationDate: {curHistRec.History.OperationDate} \n" + // дату операции
                                             $"OperationType: {curHistRec.History.OperationType} \n" + // тип операции
                                             $"CashSum: {curHistRec.History.CashSum} \n<===END\n"); // cумму операции
            }
        }
        public void GetIncomingOperationsHistory()
        {
            var userAccountHistory = History.Where(entry => entry.OperationType == OperationType.InputCash)
                                     .Join(Accounts, // соединяем History с Accounts 
                                     hist => hist.AccountId, 
                                     acc => acc.Id,
                                     (hist, acc) => new { History = hist, Account = acc })
                                     .Join(Users,  // полученное множество соединяем с Users 
                                     acc => acc.Account.UserId,
                                     usr => usr.Id,
                                     (acc,usr) => new {Account = acc,  Users = usr});
            System.Console.WriteLine($"The list of incoming operations below:\n");
            foreach (var curAccountsHist in userAccountHistory)
            {
                System.Console.WriteLine($"Start===>\nOperationDate: {curAccountsHist.Account.History.OperationDate} \n" +
                                            $"OperationType: {curAccountsHist.Account.History.OperationType} \n" +
                                            $"CashSum: {curAccountsHist.Account.History.CashSum} \n" +
                                            $"Account Owner: ID:{curAccountsHist.Users.Id} {curAccountsHist.Users.FirstName}-{curAccountsHist.Users.SurName}\n<===END\n");
            }
        }
        public void GetUsersInfoWithCashMoreThan(decimal cashValue)
        {
            var userAccounts = Accounts.Where(acc => acc.CashAll > cashValue)
                              .Join(Users,
                              account => account.UserId,
                              user => user.Id,
                              (account, user) => new {Account = account, Users = user }
                              ).ToList();
            System.Console.WriteLine($"The list of users which have account with amount more than {cashValue}\n");
            foreach(var curUserAccount in userAccounts)
            {
                System.Console.WriteLine($"UserInfo: ID:{curUserAccount.Users.Id} {curUserAccount.Users.FirstName}-{curUserAccount.Users.SurName}" +
                                         $"\nAccountInfo: ID:{curUserAccount.Account.Id} Amount:{curUserAccount.Account.CashAll}\n");
            }
        }
    }
}